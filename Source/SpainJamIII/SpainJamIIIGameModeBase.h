// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpainJamIIIGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPAINJAMIII_API ASpainJamIIIGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
